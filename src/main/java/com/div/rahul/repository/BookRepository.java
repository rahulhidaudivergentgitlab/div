package com.div.rahul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.div.rahul.model.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}
