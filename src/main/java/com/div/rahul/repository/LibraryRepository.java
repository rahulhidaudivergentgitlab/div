package com.div.rahul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.div.rahul.model.Library;
@Repository

public interface LibraryRepository extends JpaRepository<Library, Integer> {
	
	@Query(value ="SELECT t.name as name, t.id as id FROM library t WHERE t.name =?1",nativeQuery = true)
	public Library getCustomList(String s);
	
	@Query(value ="SELECT t.name as name, t.id as id FROM library t WHERE t.name = 'HelloKoding'",nativeQuery = true)
	public Library getCustomList();
}
