package com.div.rahul.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.div.rahul.model.Library;
import com.div.rahul.model.Role;

public interface RoleRepository extends JpaRepository <Role, Long> {

}
