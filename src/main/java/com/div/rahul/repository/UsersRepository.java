package com.div.rahul.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.div.rahul.dao.UserDao;
import com.div.rahul.model.DAOUser;
@Repository
public interface UsersRepository extends CrudRepository<DAOUser, Long> {
	DAOUser findByUsername(String username);
	
	@Query(value="SELECT book.*, library.name as lib_name FROM book LEFT JOIN library on library.id = book.library_id ",nativeQuery = true)
	List<Map<String, String>> getUserList();
	
}
