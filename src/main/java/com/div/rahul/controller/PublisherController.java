package com.div.rahul.controller;

import com.div.rahul.model.Library;
import com.div.rahul.model.Publisher;
import com.div.rahul.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;


@RestController
@RequestMapping("/api/v1/publishers")
public class PublisherController {

    private  PublisherRepository publisherRepository;

    @Autowired
    public PublisherController(PublisherRepository publisherRepository){
        this.publisherRepository = publisherRepository;
    }

    @PostMapping
    public ResponseEntity<Object> create(@Valid @RequestBody Publisher publisher) {
        Publisher savedPublisher = publisherRepository.save(publisher);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedPublisher.getId()).toUri();
        return ResponseEntity.created(location).body(savedPublisher);
    }

    @PutMapping("{id}")
    public  ResponseEntity<Object> update(@PathVariable Integer id , @Valid @RequestBody Publisher publisher){
        Optional savedPublisher = publisherRepository.findById(id);
        if(!savedPublisher.isPresent()){
            return  ResponseEntity.unprocessableEntity().build();
        }
        publisher.setId(id);
        publisherRepository.save(publisher);
        return ResponseEntity.noContent().build();
    }
    
    @DeleteMapping("{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id){
    	Optional savedPublisher = publisherRepository.findById(id);
        if(!savedPublisher.isPresent()){
            return  ResponseEntity.unprocessableEntity().build();
        }
        publisherRepository.deleteById(id);
    	return ResponseEntity.noContent().build();
    }
    
    @GetMapping("{id}")
    public ResponseEntity<Object> get(@PathVariable Integer id){
    	Optional savedPublisher = publisherRepository.findById(id);
    	if(!savedPublisher.isPresent()) {
    		return ResponseEntity.notFound().build();
    	}
    	return ResponseEntity.ok(savedPublisher);
    }
     
    @GetMapping
    public ResponseEntity<Object> get(){
    	return ResponseEntity.ok(publisherRepository.findAll());
    }

}
