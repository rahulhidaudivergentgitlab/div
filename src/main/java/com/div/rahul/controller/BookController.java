package com.div.rahul.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.div.rahul.model.Book;
import com.div.rahul.model.Library;
import com.div.rahul.repository.BookRepository;
import com.div.rahul.repository.LibraryRepository;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
	private final LibraryRepository libraryRepository;
    private final BookRepository bookRepository;
    
    @Autowired
    public BookController(LibraryRepository libraryRepository, BookRepository bookRepository) {
        this.libraryRepository = libraryRepository;
        this.bookRepository = bookRepository;
    }
    
    @PostMapping
    public ResponseEntity<Book> create(@Valid @RequestBody Book book) {
    	
    	if(book.getLibrary() == null ) {
    		System.out.println("(book.getLibrary() is NULL - "+book.getLibrary()+" "+book);
    		return ResponseEntity.unprocessableEntity().build();
    	}
    	
    	Optional<Library> optionalLibrary = libraryRepository.findById(book.getLibrary().getId());
        if (!optionalLibrary.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        //book.setLibrary(optionalLibrary.get());

        Book savedBook = bookRepository.save(book);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
            .buildAndExpand(savedBook.getId()).toUri();

        return ResponseEntity.created(location).body(savedBook);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity <Book> update( @PathVariable Integer id, @Valid @RequestBody Book book ){
    	 Optional<Library> optionalLibrary = libraryRepository.findById(book.getLibrary().getId());
         if (!optionalLibrary.isPresent()) {
             return ResponseEntity.unprocessableEntity().build();
         }

         Optional<Book> optionalBook = bookRepository.findById(id);
         if (!optionalBook.isPresent()) {
             return ResponseEntity.unprocessableEntity().build();
         }

         //book.setLibrary(optionalLibrary.get());
         book.setId(optionalBook.get().getId());
         bookRepository.save(book);

         return ResponseEntity.noContent().build();
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Book> delete(@PathVariable Integer id){
    	Optional<Book> optionalBook = bookRepository.findById(id);
        if (!optionalBook.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        bookRepository.delete(optionalBook.get());

        return ResponseEntity.noContent().build();
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Book> getById(@PathVariable Integer id){
    	Optional<Book> optionalBook = bookRepository.findById(id);
        if (!optionalBook.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        
        return ResponseEntity.ok(optionalBook.get());
    }
    
    @GetMapping
    public ResponseEntity<Page<Book>> getAll(Pageable pageable ){
    	return ResponseEntity.ok(bookRepository.findAll(pageable));
    }
}
