package com.div.rahul.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.div.rahul.model.Role;
import com.div.rahul.repository.RoleRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/roles")
public class RolesController {
	
	@Autowired
	RoleRepository roleRepository;
	
	@GetMapping()
	public ResponseEntity<List<Role>> get() {
		return ResponseEntity.ok(roleRepository.findAll());
	}
	
	@PostMapping()
	public ResponseEntity <Role> add(@Valid @RequestBody Role role){
		Role savedRole = roleRepository.save(role);
		return ResponseEntity.ok(savedRole);
	}
	
	@PutMapping("/{id}")
    public ResponseEntity <Role> update( @PathVariable Long id, @Valid @RequestBody Role role ){
    	Optional<Role> optionalrole = roleRepository.findById(id);
    	if(!optionalrole.isPresent()) {
    		return ResponseEntity.unprocessableEntity().build();
    	}
    	role.setId(id);
		Role savedRole = roleRepository.save(role);
		return ResponseEntity.ok(savedRole);
	}
	
	@DeleteMapping("/{id}")
    public ResponseEntity <Role> delete( @PathVariable Long id ){
    	Optional<Role> optionalrole = roleRepository.findById(id);
    	if(!optionalrole.isPresent()) {
    		return ResponseEntity.unprocessableEntity().build();
    	}
    	roleRepository.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
}
