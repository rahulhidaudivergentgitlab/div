package com.div.rahul.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TestController {
	
	@RequestMapping(method = RequestMethod.GET,value="/")
    public String index( @RequestParam(name="name",required=false,defaultValue="Rahul") String name, Model model ){
		model.addAttribute("name", name);
        return "test";
    }

}
