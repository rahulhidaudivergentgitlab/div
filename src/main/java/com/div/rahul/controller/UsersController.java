package com.div.rahul.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.div.rahul.service.UsersService;
import com.div.rahul.dao.UserDao;
import com.div.rahul.model.DAOUser;
import com.div.rahul.model.UserDTO;
import com.div.rahul.repository.LibraryRepository;
import com.div.rahul.repository.UsersRepository;

@RestController
@CrossOrigin
public class UsersController {
	@Autowired
	UsersService usersService;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	LibraryRepository libraryRepository ;
	
	@GetMapping("/users")
	public  List<DAOUser> getUsers() {
		return usersService.getAllUsers();
	}
	
	
//	@GetMapping("/test")
//	public ResponseEntity test(@RequestBody Map<String, Object> res) {
//		Map <String,Object> response = new HashMap();
//		response.put("Name", res.get("name"));
//		response.put("Values", Arrays.asList(1,2,3,4) );
//		return ResponseEntity.ok(response);
//	}
//	@RequestMapping("/test")
//	public ResponseEntity test(@RequestParam Map<String, Object> res) {
//		Map <String,Object> response = new HashMap();
//		response.put("Name", res.get("name"));
//		response.put("Values", Arrays.asList(1,2,3,4) );
//		return ResponseEntity.ok(response);
//	}
//	@RequestMapping("/test")
//	public ResponseEntity<Object> test(@RequestParam Map<String, Object> res) {
//		Map <String,Object> response = new HashMap();
//		response.put("List", libraryRepository.getCustomList("Rahul"));
//		return ResponseEntity.ok(response);
//	}
//	@RequestMapping("/test")
//	public ResponseEntity<Object> test(@RequestParam Map<String, Object> res) {
//		List s = usersRepository.getUserList();
//		
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		UserDTO customUser = (UserDTO) authentication.getPrincipal();
//		int userId = customUser.getId();
//		System.out.println(userId);
//		
//		return ResponseEntity.ok(s);
//	}
	@RequestMapping("/user")
	public ResponseEntity<Object> test(@RequestParam Map<String, Object> res) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object customUser =  authentication.getPrincipal();
		return ResponseEntity.ok(customUser);
	}
	
	@PreAuthorize("hasAuthority('ADMIN') or hasRole('ADMIN')")
	@RequestMapping("/checkUser")
	public ResponseEntity<Object> checkUser(@RequestParam Map<String, Object> res) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object customUser =  authentication.getPrincipal();
		return ResponseEntity.ok(customUser);
	}
	
//	@Autowired
//    private JavaMailSender javaMailSender;
//	
//	@RequestMapping("/mail")
//	public ResponseEntity<Object> mail(@RequestParam Map<String, Object> res) {
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		Object customUser =  authentication.getPrincipal();
//		
//		SimpleMailMessage msg = new SimpleMailMessage();
//        msg.setTo("rahul.hidau@divergentsl.com", "rahul.hidau@divergentsl.com");
//
//        msg.setSubject("Testing from Spring Boot");
//        msg.setText("Hello World \n Spring Boot Email");
//
//        javaMailSender.send(msg);
//		
//		return ResponseEntity.ok(customUser);
//	}
	//@Cacheable("addresses")
	@PostMapping("/test")
	public ResponseEntity<?> handleFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file ) {

		// Root Directory.
	      String uploadRootPath = ("upload");
	      System.out.println("uploadRootPath=" + uploadRootPath);
	      
	      File uploadRootDir = new File(uploadRootPath);
	     // Create directory if it not exists.
	      if (!uploadRootDir.exists()) {
	         uploadRootDir.mkdirs();
	      }
	      
	      // Client File Name
         String name = file.getOriginalFilename();
         System.out.println("Client File Name = " + name);
         
         if (name != null && name.length() > 0) {
             try {
                // Create the file at server
                File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(file.getBytes());
                stream.close();
                //
                
                System.out.println("Write file: " + serverFile);
             } catch (Exception e) {
                System.out.println("Error Write file: " + name);
               
             }
          }
	    return ResponseEntity.ok("File uploaded successfully.");
	  }
}
