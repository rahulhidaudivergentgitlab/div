package com.div.rahul.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import java.util.Set;

@Entity
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	private String name;
	
	//@ManyToOne(fetch=FetchType.LAZY,optional = false)
	/*@JoinColumn(name="library_id")
	@JsonProperty(access=JsonProperty.Access.WRITE_ONLY)
	private Library library;*/

	@ManyToOne(fetch=FetchType.LAZY,optional = false)
	@JoinColumn(name="library_id")
	@JsonProperty(access=JsonProperty.Access.WRITE_ONLY)
	private Library library;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "book_publisher",
			joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "publisher_id", referencedColumnName = "id"))
	private Set<Publisher> publishers;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Library getLibrary() {
		return this.library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}
	
	
	
}
