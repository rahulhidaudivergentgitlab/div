package com.div.rahul.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.div.rahul.repository.UsersRepository;
import com.div.rahul.dao.UserDao;
import com.div.rahul.model.DAOUser;

@Service
public class UsersService {
	@Autowired
	UsersRepository usersRepository; 
	
	public List<DAOUser> getAllUsers()   
	{  
		List<DAOUser> users = new ArrayList<DAOUser>();  
		usersRepository.findAll().forEach(users1 -> users.add(users1));  
		return users;  
	} 
}
