package com.div.rahul.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.div.rahul.dao.UserDao;
import com.div.rahul.repository.UsersRepository;
import com.div.rahul.dao.UserDao;
import com.div.rahul.model.DAOUser;
import com.div.rahul.model.Privilege;
import com.div.rahul.model.Role;
import com.div.rahul.model.UserDTO;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	UserDao userDao;
	
	@Autowired 
	PasswordEncoder bcryptEncoder;
	
	private static final String rolePrefix = "ROLE_";

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		DAOUser user = userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		//return new UserDTO(user);
		return new org.springframework.security.core.userdetails.User(
		          user.getUsername(), user.getPassword(),getAuthorities(user.getRoles()));
	}
	
	private Collection<? extends GrantedAuthority> getAuthorities(
      Collection<Role> roles) {
 
        return getGrantedAuthorities(getPrivileges(roles));
    }
	private List<String> getPrivileges(Collection<Role> roles) {
		 
        List<String> privileges = new ArrayList<>();
        List<Privilege> collection = new ArrayList<>();
        for (Role role : roles) {
        	privileges.add(this.rolePrefix + role.getName());
            collection.addAll(role.getPrivileges());
        }
        for (Privilege item : collection) {
            privileges.add(item.getName());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
    
	
	  public DAOUser save(DAOUser user) { 
		  DAOUser data = userDao.findByUsername(user.getUsername());
		  if(data == null) {
			  user.setId(0);
			  user.setPassword(bcryptEncoder.encode(user.getPassword())); 
			  return userDao.save(user); 
		  }else {
			  user.setId(data.getId());
			  user.setPassword(bcryptEncoder.encode(user.getPassword())); 
			  return userDao.save(user);
		  }
	  }
	 
}
